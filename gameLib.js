function randomInRange() {
    const steps = [  -100, 100, 300, 400];
    const randomStep = steps[Math.floor(Math.random() * steps.length)];
    return randomStep;
}
function randomSpritePosition() {
    const spriteWidth = 100; // กำหนดความกว้างของ sprite เป็น 100 pixels
    const spriteHeight = 100; // กำหนดความสูงของ sprite เป็น 100 pixels
    const gameWidth = 1500; // กำหนดความกว้างของเกมเป็น 1500 pixels
    const gameHeight = 900; // กำหนดความสูงของเกมเป็น 900 pixels

    // สุ่มตำแหน่ง x และ y ของ sprite
    let randomX = Math.floor(Math.random() * (gameWidth - spriteWidth));
    let randomY = Math.floor(Math.random() * (gameHeight - spriteHeight));
    console.log(randomY)
    randomY = randomY>100?randomY:randomY+100;
    console.log(randomY)
    return { x: randomX, y: randomY };
}

function Enemy() {
    tEnemy = new Sprite(game, "ship.png", 50, 30);
    tEnemy.setSpeed(3);
    tEnemy.setPosition(game.width / 2 + 800, game.height / 2 - randomInRange());
    tEnemy.setAngle(315);
    let seconds = 5;
    tEnemy.lastFireTime = 0;
    tEnemy.isDead = false;
    tEnemy.init = true;
    tEnemy.launch = 6000;
    setTimeout(() => {
        tEnemy.init = false;
    }, 1000);

    tEnemy.dead = function () {
        this.isDead = true;
    }
    tEnemy.fire = function () {
        var currentTime = new Date().getTime();
        if (currentTime - this.lastFireTime >= this.launch && this.isDead == false && this.init == false) {
            console.log("check");
            this.lastFireTime = currentTime;
            missileEnemy.fire();

        }

    }

    tEnemy.checkBounds = function () {

        if (this.x < 200) { // ตำแหน่งศัตรูเข้ามาไม่เกิน 200 px จากขอบซ้าย
            this.xspeed = 3;
            this.setAngle(45);
        }
        if (this.x > game.width - 200) { // ตำแหน่งศัตรูเข้ามาไม่เกิน 200 px จากขอบขวา
            this.xspeed = -3;
            this.setAngle(315);
        }
        if (this.y < 200) { // ตำแหน่งศัตรูเข้ามาไม่เกิน 200 px จากขอบบน
            this.yspeed = 3;
            this.setAngle(225);
        }
        if (this.y > game.height - 200) { // ตำแหน่งศัตรูเข้ามาไม่เกิน 200 px จากขอบล่าง
            this.yspeed = -3;
            this.setAngle(135);
        }
        var dx = car.x - this.x;
        var dy = car.y - this.y;
        var angle = Math.atan2(dy, dx) * 180 / Math.PI;
        this.setAngle(angle + 90);
    }; // end checkBounds



    return tEnemy;
}


function Car() {
    tCar = new Sprite(game, "tank.png", 50, 30);
    tCar.setSpeed(5);
    tCar.setPosition(50,50);
    tCar.setAngle(135);
    tCar.lastFireTime = 0;
    tCar.keyHeld = false;
    tCar.setBoundAction(BOUNCE); // เพิ่มบรรทัดนี้เพื่อให้รถหยุดเ
    tCar.checkClick = function () {
        if (this.isClicked()) {
            missile.fire();
            console.log("Click");
        } // end if
    }
    tCar.checkDrag = function () {
        speed = this.getSpeed();
        speed *= .95;
        this.setSpeed(speed);
    }
    tCar.checkKeys = function () {
        // this.setBoundAction(STOP);
        if (keysDown[K_A]) {
            this.changeAngleBy(-5);
        }

        if (keysDown[K_D]) {
            this.changeAngleBy(5);
        }

        if (keysDown[K_W]) {
            this.addVector(this.getImgAngle(), 0.8);
        }

        if (keysDown[K_SPACE]) {
            this.fireMissile();
        }

        // Check mouse click event
        game.canvas.addEventListener('mousedown', function () {
            tCar.fireMissile();
        });

        // Fire missile function
        this.fireMissile = function () {
            currentTime = new Date().getTime();
            if (!this.keyHeld && currentTime - this.lastFireTime >= 4000) {
                missile.fire();
                this.lastFireTime = currentTime;
                this.keyHeld = true;
            } else {
                this.keyHeld = false;
            }
        }
    } // end checkKeys
    return tCar;
} // end car def// end car def
function Coin() {
    const spritePosition = randomSpritePosition();
    tCoin = new Sprite(game, "coin.png", 215, 42);
    tCoin.loadAnimation(215, 42, 215 / 5.9, 42);
    tCoin.generateAnimationCycles();
    tCoin.renameCycles(["1"]);
    tCoin.setAnimationSpeed(1500);
    tCoin.playAnimation();
    tCoin.setCurrentCycle("1");
    tCoin.setSpeed(0);
    // tCoin.setPosition(game.width / 2, game.height / 2);
    tCoin.setPosition(spritePosition.x, spritePosition.y);
    return tCoin;
}
function Missile(speed) {
    tMissile = new Sprite(game, "missile.png", 30, 20);
    tMissile.hide();
    tMissile.fire = function () {
        this.show();
        var soundMissile = new Sound("launch.mp3");
        soundMissile.play();
        this.setSpeed(speed);
        this.setBoundAction(DIE);
        this.setPosition(car.x, car.y);
        this.setAngle(car.getImgAngle());


    } // end fire


    return tMissile;
} // end missile def
function MissileEnemy(speed) {
    tMissileEnemy = new Sprite(game, "missile.png", 30, 20);
    tMissileEnemy.hide();
    tMissileEnemy.fire = function () {
        this.show();
        var soundMissile = new Sound("launch.mp3");
        soundMissile.play();
        this.setSpeed(speed);
        this.setBoundAction(DIE);
        this.setPosition(enemy.x, enemy.y);
        this.setAngle(enemy.getImgAngle());


    } // end fire
    return tMissileEnemy;
} // end missile def

